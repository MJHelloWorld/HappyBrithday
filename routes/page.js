/**
 * Created by ming on 2017/6/7.
 */
const express = require('express');
const router = express.Router();
const {version, checkSign, setSign} = require('../config');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.redirect('/login');
});
// login page
router.get('/login', (req, res, next) => {
    res.render('login', {version});
});

/*middleware login request*/
//router.use((req, res, next) => {
//    let sesstion = req.session;
//    if (sesstion && sesstion.user) {
//        next();
//    } else {
//        // 记住当前链接 ，登录后重定向
//        let originalUrl = req.originalUrl;
//        req.session = req.session || {};
//        //req.session.redirectUrl = originalUrl;
//        res.redirect('/login');
//    }
//});

router.get('/BirthdayCake.html', (req, res) => {
    res.render('BirthdayCake', {version});
});

router.get('/Memories.html', (req, res) => {
    res.render('Memories', {version});
});

module.exports = router;
