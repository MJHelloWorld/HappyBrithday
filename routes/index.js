/**
 * Created by ming on 2017/6/7.
 */
const page = require('./page');
const user = require('./users');

const init = (app) => {
    app.use('/api/user', user);
    app.use('/', page);
};
module.exports = {
    init
};
