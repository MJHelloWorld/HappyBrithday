/**
 * Created by ming on 2017/6/7.
 */
const md5 = require('md5');
const version = Math.random().toString().substr(2, 6);

const setSign = (account = '') => {
    return md5(`mfmm${account}1234`);

};
const checkSign = (account = '', sign = '') => {
    return setSign(account) === sign;
};

module.exports = {
    version,
    setSign,
    checkSign
};